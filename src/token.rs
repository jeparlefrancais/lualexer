/// The different type of tokens that will be produced by the lexer
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TokenType {
    Comment,
    Identifier,
    Keyword,
    Number,
    String,
    Symbol,
    Whitespace,
}

/// A small struct that contains minimal information about a slice of
/// the input. The token does not own the part of the code that it
/// represents, it only keeps a reference to the original input.
#[derive(Debug, Eq, PartialEq)]
pub struct Token<'a> {
    token_type: TokenType,
    slice: &'a str,
}

impl<'a> Token<'a> {
    fn new(token_type: TokenType, slice: &'a str) -> Token<'a> {
        Token {
            token_type,
            slice,
        }
    }

    pub fn new_comment(slice: &'a str) -> Token<'a> {
        Token::new(TokenType::Comment, slice)
    }

    pub fn new_identifier(slice: &'a str) -> Token<'a> {
        Token::new(TokenType::Identifier, slice)
    }

    pub fn new_keyword(slice: &'a str) -> Token<'a> {
        Token::new(TokenType::Keyword, slice)
    }

    pub fn new_number(slice: &'a str) -> Token<'a> {
        Token::new(TokenType::Number, slice)
    }

    pub fn new_string(slice: &'a str) -> Token<'a> {
        Token::new(TokenType::String, slice)
    }

    pub fn new_symbol(slice: &'a str) -> Token<'a> {
        Token::new(TokenType::Symbol, slice)
    }

    pub fn new_whitespace(slice: &'a str) -> Token<'a> {
        Token::new(TokenType::Whitespace, slice)
    }

    pub fn get_type(&self) -> &TokenType {
        &self.token_type
    }

    pub fn get_content(&self) -> &str {
        &self.slice
    }

    pub fn is_type(&self, token_type: TokenType) -> bool {
        self.token_type == token_type
    }

    pub fn is_slice<'b>(&self, compare_with: &'b str) -> bool {
        self.slice == compare_with
    }
}
