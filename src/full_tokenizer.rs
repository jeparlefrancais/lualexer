use crate::lexer::Lexer;
use crate::token::Token;
use crate::tokenizer::Tokenizer;
use crate::utils::{
    accumulate_while,
    is_whitespace,
};

#[derive(Debug, Default, Clone)]
pub struct FullTokenizer;

impl Tokenizer for FullTokenizer {
    fn new() -> FullTokenizer {
        FullTokenizer {}
    }

    fn process_whitespace<'a>(&self, tokens: &mut Vec<Token<'a>>, input: &'a str) -> &'a str {
        match input.chars().next() {
            Some(first_character) if is_whitespace(first_character) => {
                let (whitespace, next_input) = accumulate_while(input, is_whitespace);
                tokens.push(Token::new_whitespace(whitespace));

                next_input
            },
            _ => input,
        }
    }
}

/// A lexer that keeps the information about whitespaces.
pub type FullLexer = Lexer<FullTokenizer>;

#[cfg(test)]
mod tests {
    use super::*;

    fn tokenizer() -> FullTokenizer {
        FullTokenizer::new()
    }

    #[test]
    fn preceding_space_is_preserved() {
        let result = tokenizer().parse(" ").unwrap();

        assert_eq!(result, vec!(Token::new_whitespace(" ")));
    }

    #[test]
    fn leading_space_is_preserved() {
        let result = tokenizer().parse(" abc \n").unwrap();

        assert_eq!(result, vec!(
            Token::new_whitespace(" "),
            Token::new_identifier("abc"),
            Token::new_whitespace(" \n"),
        ));
    }
}
