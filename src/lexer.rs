use crate::token::Token;
use crate::tokenizer::Tokenizer;

/// Errors that can happen while tokenizing some input.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum LexerErrorType {
    /// While parsing something that initially appeared as a number,
    /// some invalid character screw it up.
    MalformedNumber,
    /// A string was begun (from a `'`, `"` or opening double brackets
    /// `[=*[`) but never closed. Quotes are generally expected to at the
    /// end of line, but the balanced bracket notation is used commonly
    /// used for multiline strings.
    UnfinishedString,
    /// The lexer found a symbol that was not supposed to be there.
    UnexpectedSymbol,
}

/// When the lexer encounters an error, it returns the error type
/// and the rest of the given input that was not parsed.
pub type LexerError<'a> = (LexerErrorType, &'a str);

/// This struct wraps the tokenizer to offer a simple interface to parse
/// a string.
pub struct Lexer<T: Tokenizer> {
    tokenizer: T,
}

impl<T: Tokenizer> From<T> for Lexer<T> {
    fn from(tokenizer: T) -> Lexer<T> {
        Lexer {
            tokenizer,
        }
    }
}

impl<T: Tokenizer> Lexer<T> {
    pub fn new() -> Lexer<T> {
        Lexer {
            tokenizer: T::new(),
        }
    }

    pub fn parse<'a>(&self, input: &'a str) -> Result<Vec<Token<'a>>, LexerError<'a>> {
        self.tokenizer.parse(input)
    }
}
