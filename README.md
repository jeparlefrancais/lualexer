[![pipeline status](https://gitlab.com/jeparlefrancais/lualexer/badges/master/pipeline.svg)](https://gitlab.com/jeparlefrancais/lualexer/commits/master)
[![version](https://img.shields.io/crates/v/lualexer)](https://crates.io/crates/lualexer)
[![license](https://img.shields.io/crates/l/lualexer)](LICENSE.txt)

# lualexer

A lexer that reads lua code and produces tokens.

The crate provides two different lexers:
  * `FastLexer`: skips all the whitespace tokens
  * `FullLexer`: produces every tokens

This project is in early development so things could change rapidly, as its purpose is only to
support the `luaparser` crate.

# License

lualexer is available under the MIT license. See [LICENSE.txt](LICENSE.txt) for details.
