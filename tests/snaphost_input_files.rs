use insta::assert_debug_snapshot;
use lualexer::FullLexer;
use std::fs;
use std::path::Path;

fn lexer() -> FullLexer {
    FullLexer::new()
}

fn read_file(file_name: &str, extension: &str) -> String {
    let manifest_path = env!("CARGO_MANIFEST_DIR");
    let manifest_dir = Path::new(&manifest_path);
    let file_path = manifest_dir.join("tests")
        .join("input_files")
        .join(format!("{}.{}", &file_name, extension));

    fs::read_to_string(&file_path)
        .unwrap_or_else(|reason| panic!(
            "could not read input file <{}>: {}",
            file_path.to_string_lossy(),
            reason))
}

macro_rules! test_input_file {
    ($file_name:ident) => {
        test_input_file!($file_name, "lua");
    };
    ($file_name:ident, $extension:literal) => {
        #[test]
        fn $file_name() {
            let file = stringify!($file_name);
            let content = read_file(file, $extension);
            let tokens = lexer().parse(&content).unwrap();

            assert_debug_snapshot!(file, tokens);
        }
    };
}

test_input_file!(comments);
test_input_file!(for_loop);
test_input_file!(numbers, "txt");
test_input_file!(strings);
test_input_file!(table);
